import React from "react";
import { Navbar, Form } from "react-bootstrap";

const NavBar = ({ saldofinal }) => {
  return (
    <Navbar bg="light" expand="lg">
      <Navbar.Brand href="#home">
        <img
          alt=""
          src="https://i.pinimg.com/736x/c1/7a/75/c17a7540e1c301e7648e242bf0c0a5ab.jpg"
          width="30"
          height="30"
          className="d-inline-block align-top"
        />{" "}
        Presupuesto
      </Navbar.Brand>
      <Navbar.Toggle />
      <Navbar.Collapse className="justify-content-end">
      <Form inline>
      <Form.Label className="mr-sm-2" >Saldo Inicial:</Form.Label>
      <Form.Control type="text" placeholder="" className="mr-sm-2" />
      <Form.Label className="mr-sm-2" >Saldo Final:</Form.Label>
      <Form.Control type="text" placeholder="" className="mr-sm-2" value ={saldofinal} readOnly />
    </Form>
      </Navbar.Collapse>
    </Navbar>


);
};

export default NavBar;
