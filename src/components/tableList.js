import React from "react";
import {
  Table,
  Row,
  Col,
  Container,
  Form,
  Badge,
  Button,
} from "react-bootstrap";

import { RiDeleteBin2Line } from "react-icons/ri";
import ModalModificar from "./modalModificar";

const TableList = ({ registros, removeRegistro , editRegistro }) => {
  return (
    <Container>
      <Row>
        <Col>
          <Form.Label column sm={10}>
            Lista de moviemientos
          </Form.Label>
          <Badge variant="primary">{registros.length}</Badge>
        </Col>
      </Row>
      <Row>
        <Col>
          <Table>
            <tbody>
              {registros.map((item) => (
                <tr key={item.id}>
                  <td>
                  <ModalModificar registros = {item} editRegistro = {editRegistro}/>
                    <Button
                      onClick={() => {
                        removeRegistro(item.id);
                      }}
                      variant="primary"
                    >
                      <RiDeleteBin2Line />
                    </Button>
                  </td>
                  <td>{item.name}</td>
                  <td>
                    <Badge variant={item.type === 'Ingreso' ? "success" : "danger"}>{item.count}</Badge>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        </Col>
      </Row>
    </Container>
  );
};

export default TableList;
