import { Col, Container, Row } from "react-bootstrap";
import NavBar from "./components/navBar";
import FormCrear from "./components/formCrear";
import React, { useState } from "react";
import TableList from "./components/tableList";

const App = () => {
  const [registros, setregistros] = useState([]);
  const [saldofinal, setsaldofinal] = useState(0);

  const addRegistro = (registro) => {
    setregistros([...registros, registro]);
  };

  const removeRegistro = (id) => {
    setregistros(registros.filter((registro) => registro.id !== id));
  };

  const editRegistro = (id, newregistro) => {
    const newRegistroEdit = registros.map((registro) => {
      if (registro.id === id) {
        return { ...registro, name: newregistro.name, count: newregistro.count, type: newregistro.type };
      }
      return registro;
    });
    setregistros(newRegistroEdit);
  };

  return (
    <Container>
      <NavBar  saldofinal={saldofinal} />
      <br />
      <Row>
        <Col>
          <FormCrear addRegistro={addRegistro} />
          </Col>
          <Col>
          <TableList 
          registros={registros} 
          removeRegistro={removeRegistro} 
          editRegistro = {editRegistro}
          />
        </Col>
      </Row>
    </Container>
  );
};
export default App;
