import { Button, Container, Row, Col, Modal, Form } from "react-bootstrap";
import "../style/styles.css";

import React, { useState } from "react";
import { uuid } from "uuidv4";

const FormCrear = ({ addRegistro }) => {
  const [registro, setregistro] = useState({
    id: "",
    name: "",
    count: "",
    type: "",
  });

  const handleImputChange = (e) => {
    setregistro({ ...registro, [e.target.name]: e.target.value });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    if (registro.name.trim()) {
      addRegistro({ ...registro, id: uuid() });
    }
    //Reset form
    setregistro ({...registro, name:"",count:"",type:"Ingreso"})
  };

  const handleCancelar = (e) => {
    e.preventDefault();
    //Reset form
    setregistro ({...registro, name:"",count:"",type:"Ingreso"})
  };

  return (
    <Container>
      <Form.Group as={Row} controlId="formGridState">
        <Form.Label column sm={6}>
          Registro
        </Form.Label>
      </Form.Group>
      <Form.Group as={Row} controlId="formGridState">
        <Form.Label column sm={4}>
          Tipo Movimiento
        </Form.Label>
        <Col sm={8}>
          <Form.Control
            as="select"
            type="text"
            placeholder=""
            name="type"
            value={registro.type}
            onChange={handleImputChange}
            defaultValue="Seleccionar"
          >
            <option>Ingreso</option>
            <option>Gasto</option>
          </Form.Control>
        </Col>
      </Form.Group>
      <Form.Group as={Row} controlId="formNombre">
        <Form.Label column sm={2}>
          Nombre
        </Form.Label>
        <Col sm={10}>
          <Form.Control
            type="text"
            placeholder=""
            name="name"
            value={registro.name}
            onChange={handleImputChange}
          />
        </Col>
      </Form.Group>
      <Form.Group as={Row} controlId="formCantidad">
        <Form.Label column sm={2}>
          Cantidad
        </Form.Label>
        <Col sm={10}>
          <Form.Control
            type="text"
            placeholder=""
            name="count"
            value={registro.count}
            onChange={handleImputChange}
          />
        </Col>
      </Form.Group>
      <Form.Group as={Row} controlId="formCantidad">
      <Col sm={5}>
      <Button  onClick={handleCancelar} variant="primary">Cancelar</Button>
      </Col>
      <Col sm={5}>
      <Button  onClick={handleSubmit} variant="primary">
        Agregar Movimiento
      </Button>
      </Col>
      </Form.Group>
    </Container>
  );
};

export default FormCrear;
